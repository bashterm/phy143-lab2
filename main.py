import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np
from scipy.optimize import curve_fit

df = pd.read_csv('phy143lab2data_correct.csv', header=[0,1])
#df.columns = df.columns.map('_'.join)

def get_label(run):
    return 'Run #{}'.format(run)

for i in range(1,50):
    label = get_label(i)
    df.drop([(label,'Angle (rad)'), (label,'Angular Velocity (rad/s)'),(label,'Angular Acceleration (rad/s²)'), (label,'Velocity (m/s)'),(label,'Acceleration (m/s²)')], axis='columns', inplace=True)

def export_data():
    df.to_csv('phy143lab2dataCull.csv')

def get_x_y(run):
    label = 'Run #{}'.format(run)
    x = df[label]['Position (m)'].to_numpy()
    y = df[label]['Light Intensity (%)'].to_numpy()
    x = x[~np.isnan(x)]
    y = y[~np.isnan(y)]
    return x,y

def graph_light(run, *runs):
    label = 'Run #{}'.format(run)
    plt.plot(df[label]['Position (m)'], df[label]['Light Intensity (%)'])
    for i in runs:
        label = get_label(i)
        plt.plot(df[label]['Position (m)'], df[label]['Light Intensity (%)'])
    plt.show()

def single_slit(x, slit_width, x0, wavelength, distance, max_intensity, y0):
    argument = (math.pi*slit_width*(x- x0))/(wavelength*distance)

    return max_intensity*(np.sin(argument) / argument)**2 + y0

init_vals=[0.04e-02,0.07,650e-9,35e-2,50,0.0] # for trial 1
x,y = get_x_y(1)
best_vals, covar = curve_fit(single_slit,x,y,p0=init_vals)
fit = single_slit(x,best_vals[0],best_vals[1],best_vals[2],best_vals[3],best_vals[4],best_vals[5])
